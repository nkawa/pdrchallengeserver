package jp.hasc.pdrserver;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Writer;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

//import jp.hasc.hasctool.core.util.CoreUtil;


import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
//import org.eclipse.core.resources.IFile;
//import org.eclipse.core.runtime.CoreException;

public class HascUploaderServlet extends HttpServlet
{
	/** logger for this class */
//	private final static org.apache.commons.logging.Log LOG = org.apache.commons.logging.LogFactory
//			.getLog(HascUploaderServlet.class);
	
	private static final String TEMP_NAME = "PDR_temp";
	
	//pdrチャレンジの親ディレクトリの場所
	private static final String PARENT_DIR = "/pdrchallenge";
	//このディレクトリにアップロードされたデータが入っていく
	private static final String RAW_DIR ="raw-data";
	//抽出したセンサデータ群を格納するディレクトリ
	private static final String UNPROCESSED_DIR = "unprocessed";

	private static final long serialVersionUID = 1L;

	//public static IFolder basefolder = null;
	private File tempDir_, pdrDir_,unprocessedDir_;


	//アップロードされるファイルの種類を表す定数
	private final int TYPE_CALIBRATION = 1;
	private final int TYPE_SENSOR = 2;
	private final int TYPE_BUTTON = 3;

	public HascUploaderServlet(){
	}
	
	@Override
	public void init() throws ServletException {
		// temp dir
		tempDir_=new File(System.getProperty("java.io.tmpdir"),TEMP_NAME);
		tempDir_.mkdirs();
		
		// for PDR Dir (should make it configurable..)
		File parent = new File(PARENT_DIR,"pdrdata");
		
		pdrDir_ = new File(parent,RAW_DIR);
		pdrDir_.mkdirs();
		
		unprocessedDir_ = new File(parent,UNPROCESSED_DIR);
		unprocessedDir_.mkdirs();		
		
	}
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		response.setContentType("text/plain");
		response.getWriter().print("PDR Challenge Server. It is working.");
//		response.getWriter().print("Working Tmp Dir:"+tempDir_.toString());
		response.setStatus(HttpServletResponse.SC_OK);
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse response)
	throws ServletException, IOException {
		if (!ServletFileUpload.isMultipartContent(req)) {
			response.setContentType("text/plain");
			response.getWriter().print("Not multipart content");
			response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
			return;
		}
		
		/*{
			// for debug
			byte[] buff=new byte[65536];
			ServletInputStream inps = req.getInputStream();
			inps.read(buff,0,buff.length);
			String s=new String(buff);
			System.out.println(s);
		}*/
		
		try {
			// Create a factory for disk-based file items
			DiskFileItemFactory factory = new DiskFileItemFactory();
			//factory.setSizeThreshold(0);
			factory.setRepository(tempDir_);
	
			// Create a new file upload handler
			ServletFileUpload upload = new ServletFileUpload(factory);
	
			// Parse the request
			@SuppressWarnings("unchecked")
			List<FileItem> items = upload.parseRequest(req);
			for(FileItem item : items) {
			    String fieldName = item.getFieldName();
				if (item.isFormField()) {
					// do nothing
				}else{
				    if (fieldName.equals("logfile")) {
				    	processLogFile(item);
				    }else if (fieldName.equals("audiofile")) {
				    	// not using audio...
				    	//				    	processAudioFile(item);
				    }
				}
			}
			
			//
			response.setContentType("text/plain");
			response.getWriter().print("done");
			response.setStatus(HttpServletResponse.SC_OK);
			
		} catch (Exception ex) {
			ex.printStackTrace();
			response.setContentType("text/plain");
			response.getWriter().print(ex.toString());
			response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
			return;
		}
	}
	
	private String getNewFileName(String fileName, String basename_postfix, String ext) {
		int pidx=Math.max(fileName.lastIndexOf("/"),fileName.lastIndexOf("\\"));
		if (pidx>=0) fileName=fileName.substring(pidx+1);
		//
		int idx=fileName.lastIndexOf('.');
		if (ext==null) {
			if (idx>=0) ext=fileName.substring(idx); else ext="";
		}
		if (idx>=0) {
			fileName=fileName.substring(0,idx)+basename_postfix+ext;
		}else{
			fileName=fileName+basename_postfix+ext;
		}
		return fileName;
	}
	
	private File getTempFile(String fileName,String postfix) {
		return new File(tempDir_,getNewFileName(fileName,"","")+"-"+postfix);
	}

	/* We do not use Audio in PDR Challenge!
	private void processAudioFile(FileItem item) throws Exception {
		String fileName=item.getName();
		File f=getTempFile(fileName,"audio");
		item.write(f);
		String newFileName = getNewFileName(fileName,"-audio",null);
	
		IFile outf=saveToResourceFileAndClose(new FileInputStream(f), newFileName);
		item.delete();
		f.delete();
		try{
			CoreUtil.convertAudioFile(outf);
		}catch(Exception ex) {
			LOG.warn("Audio conversion failed: "+ex.toString());
		}
	}
	*/

	private void processLogFile(FileItem item) throws Exception {
		String fileName=item.getName();
		File f=getTempFile(fileName,"logfile");
		item.write(f);
		//saveToFileAndClose(new FileInputStream(f), getNewFileName(fileName,"-raw",".log"));
		saveToFileAndClose(new FileInputStream(f), getNewFileName(fileName,"",".log"));
		saveAccFile(fileName,new FileInputStream(f));
		item.delete();
		f.delete();
	}

	private void saveAccFile(String fileName, InputStream inps) throws IOException, InterruptedException{
		BufferedReader br=new BufferedReader(new InputStreamReader(inps));
		int log_type = 0;

		//スケルトンアプリから送られてきたログと正解経路記録アプリから送られてきたログを識別する情報をファイル名から除く。メタファイルのファイル名には残す
		String full_name = fileName.toString();
		
		if(fileName.contains("-sensor")){
			log_type = TYPE_SENSOR;
			fileName = fileName.replaceAll("-sensor", "");

		}else if(fileName.contains("-button")){
			log_type = TYPE_BUTTON;
			fileName = fileName.replaceAll("-button", "");

		}else if(fileName.contains("-calibration")){
			log_type = TYPE_CALIBRATION;
			fileName = fileName.replaceAll("-calibration", "");
			fileName = "calibration_" + fileName;
		}
		
		File metaTempFile = getTempFile(full_name,"meta");
		Writer metaWriter = new FileWriter(metaTempFile);
		boolean acc = false;
		File accTempFile = getTempFile(fileName,"acc");
		Writer accWriter = new FileWriter(accTempFile);
		boolean gyro = false;
		File gyroTempFile = getTempFile(fileName,"gyro");
		Writer gyroWriter = new FileWriter(gyroTempFile);
		boolean mag = false;
		File magTempFile = getTempFile(fileName,"mag");
		Writer magWriter = new FileWriter(magTempFile);
		boolean heading = false;
		File headingTempFile = getTempFile(fileName,"heading");
		Writer headingWriter = new FileWriter(headingTempFile);
		boolean loc = false;
		File locTempFile = getTempFile(fileName,"loc");
		Writer locWriter = new FileWriter(locTempFile);
		boolean pressure = false;
		File pressureTempFile = getTempFile(fileName,"pressure");
		Writer pressureWriter = new FileWriter(pressureTempFile);
		boolean light = false;
		File lightTempFile = getTempFile(fileName,"light");
		Writer lightWriter = new FileWriter(lightTempFile);
		boolean proximity = false;
		File proximityTempFile = getTempFile(fileName,"proximity");
		Writer proximityWriter = new FileWriter(proximityTempFile);

		//PDRチャレンジ用
		boolean wifi = false;
		File wifiTempFile = getTempFile(fileName, "wifi");
		Writer wifiWriter = new FileWriter(wifiTempFile);
		
		boolean correct = false;
		File correctTempFile = getTempFile(fileName, "correct");
		Writer correctWriter = new FileWriter(correctTempFile);


		//
		try {
			while(true) {
				String line = br.readLine();
				if (line==null) break;
				//System.out.println("["+line+"]");
				if(line.contains("ACC")){
					String dd[] = line.split("ACC\t");
					//System.out.println("("+dd[1]+")");
					if(dd.length >1){
						accWriter.write(dd[1]+"\n");
						if(!acc)acc = true;
					}
				}else if(line.contains("GYRO")){
					String dd[] = line.split("GYRO\t");
					//System.out.println("("+dd[1]+")");
					if(dd.length >1){
						gyroWriter.write(dd[1]+"\n");
						if(!gyro)gyro = true;
					}
				}else if(line.contains("MAG")){
					String dd[] = line.split("MAG\t");
					//System.out.println("("+dd[1]+")");
					if(dd.length >1){
						String[] row = dd[1].split(",");
						if (row.length>3) {
							magWriter.write(row[0]+","+row[1]+","+row[2]+","+row[3]+"\n");
						}else{
							magWriter.write(dd[1]+"\n");
						}
						if(!mag)mag = true;
					}
				}else if(line.contains("HEADING")){
					String dd[] = line.split("HEADING\t");
					//System.out.println("("+dd[1]+")");
					if(dd.length >1){
						String[] row = dd[1].split(",");
						//System.out.println(dd[1]);
						//System.out.println(row[0]+","+row[1]+","+row[2]+","+row[3]);
						if (row.length>3) {
							headingWriter.write(row[0]+","+row[1]+","+row[2]+","+row[3]+"\n");
							if(!heading)heading = true;
						}
					}
				}else if(line.contains("LOC")){
					String dd[] = line.split("LOC\t");
					//System.out.println("("+dd[1]+")");
					if(dd.length >1){
						locWriter.write(dd[1]+"\n");
						if(!loc)loc = true;
					}
				}else if(line.contains("PRESSURE")){
					String dd[] = line.split("PRESSURE\t");
					//System.out.println("("+dd[1]+")");
					if(dd.length >1){
						pressureWriter.write(dd[1]+"\n");
						if(!pressure)pressure = true;
					}
				}else if(line.contains("LIGHT")){
					String dd[] = line.split("LIGHT\t");
					//System.out.println("("+dd[1]+")");
					if(dd.length >1){
						lightWriter.write(dd[1]+"\n");
						if(!light)light = true;
					}
				}else if(line.contains("PROXIMITY")){
					String dd[] = line.split("PROXIMITY\t");
					//System.out.println("("+dd[1]+")");
					if(dd.length >1){
						proximityWriter.write(dd[1]+"\n");
						if(!proximity)proximity = true;
					}

				//PDRチャレンジ用-------------------------------------------
				} else if (line.contains("POS")) {//ボタン押した位置と時間
					String dd[] = line.split("POS\t");
					if (dd.length > 1) {
						correctWriter.write(dd[0].trim() + "," + dd[1] + "\n");
						if (!correct)
							correct = true;
					}
				 
				} else if (line.contains("WIFI")) {
					String dd[] = line.split("WIFI\t");
					if (dd.length > 1) {
						wifiWriter.write(dd[1] + "\n");
						if (!wifi)
							wifi = true;
					}
				//---------------------------------------------------------


				}else if(line.contains("TAGS")){
					String dd[] = line.split("TAGS\t");
					//					System.out.println("("+dd[1]+")");
					if(dd.length >1)
						metaWriter.write("Tags: "+dd[1]+"\n");					
				}else if(line.contains("LOG_VERSION")){
					String dd[] = line.split("LOG_VERSION\t");
					//					System.out.println("("+dd[1]+")");
					if(dd.length >1)
						metaWriter.write("LogVersion: "+dd[1]+"\n");					
				}else if(line.contains("TERMINAL_ID")){
					String dd[] = line.split("TERMINAL_ID\t");
					//					System.out.println("("+dd[1]+")");
					if(dd.length >1)
						metaWriter.write("TerminalID: "+dd[1]+"\n");					
				}else if(line.contains("TERMINAL_TYPE")){
					String dd[] = line.split("TERMINAL_TYPE\t");
					//					System.out.println("("+dd[1]+")");
					if(dd.length >1)
						metaWriter.write("TerminalType: "+dd[1]+"\n");					
				}else if(line.contains("COMMENT")){
					String dd[] = line.split("COMMENT\t");
					//					System.out.println("("+dd[1]+")");
					if(dd.length >1)
						metaWriter.write("Comment: "+dd[1]+"\n");					
				}else if(line.contains("CLIENT_VERSION")){
					String dd[] = line.split("CLIENT_VERSION\t");
					//					System.out.println("("+dd[1]+")");
					if(dd.length >1)
						metaWriter.write("ClientVersion: "+dd[1]+"\n");					
				}else{
					System.out.println("no output["+line+"]");
				}
			}

		} catch (IOException e) {
			e.printStackTrace();
		}finally {
			br.close();
			metaWriter.close();
			accWriter.close();
			gyroWriter.close();
			magWriter.close();
			headingWriter.close();
			locWriter.close();
			pressureWriter.close();
			lightWriter.close();
			proximityWriter.close();

			//PDRチャレンジ用-------------------
			wifiWriter.close();
			correctWriter.close();
			//--------------------------------
		}
		
		// output for IDirectory
		if(acc) saveToFileAndClose(new FileInputStream(accTempFile),getNewFileName(fileName,"_acc",".csv"));
		accTempFile.delete();
		if(gyro) saveToFileAndClose(new FileInputStream(gyroTempFile),getNewFileName(fileName,"_gyro",".csv"));
		gyroTempFile.delete();
		if(mag) saveToFileAndClose(new FileInputStream(magTempFile),getNewFileName(fileName,"_mag",".csv"));
		magTempFile.delete();
		if(heading) saveToFileAndClose(new FileInputStream(headingTempFile),getNewFileName(fileName,"_heading",".csv"));
		headingTempFile.delete();
		if(loc) saveToFileAndClose(new FileInputStream(locTempFile),getNewFileName(fileName,"_loc",".csv"));
		locTempFile.delete();
		if(pressure) saveToFileAndClose(new FileInputStream(pressureTempFile),getNewFileName(fileName,"_pressure",".csv"));
		pressureTempFile.delete();
		if(light) saveToFileAndClose(new FileInputStream(lightTempFile),getNewFileName(fileName,"_light",".csv"));
		lightTempFile.delete();
		if(proximity) saveToFileAndClose(new FileInputStream(proximityTempFile),getNewFileName(fileName,"_proximity",".csv"));
		proximityTempFile.delete();
		//
		if(wifi) saveToFileAndClose(new FileInputStream(wifiTempFile),getNewFileName(fileName, "_wifi",".csv"));
		wifiTempFile.delete();
		
		if (correct)saveToFileAndClose(new FileInputStream(correctTempFile),getNewFileName(fileName, "_correct", ".csv"));
		correctTempFile.delete();

		saveToFileAndClose(new FileInputStream(metaTempFile),getNewFileName(full_name,"",".meta"));
		metaTempFile.delete();

		
		//センサ値分割保存終了を表すフラグファイルを出力
		String fin_file_name = fileName.toString();
		fin_file_name = fin_file_name.replace(".log", "");
		
		switch(log_type){
			case TYPE_SENSOR:
			fin_file_name += "_finsen.csv";
			break;
			
			case TYPE_CALIBRATION:
			fin_file_name += "_fin.csv";
			break;
			
			case TYPE_BUTTON:
			fin_file_name += "_fincor.csv";
			break;
			default:
		}
		
		saveToFileAndClose(new FileInputStream(getTempFile("hoge",".txt")),fin_file_name);

	}

	private void saveToFileAndClose(InputStream inps,String fileName) throws IOException, InterruptedException{
		
		File pdrf;
		if(fileName.contains(".csv"))
			pdrf = new File(unprocessedDir_, fileName);
		else
			pdrf = new File(pdrDir_, fileName);
		
		
		System.out.println("PDR Challenge Server: Try to save "+pdrf.toString());
		FileOutputStream fos = new FileOutputStream(pdrf);
		int c,len;
		byte b[]= new byte[1024];
		while((c = inps.available())> 0){
			if( c > 1024) c= 1024;
			len= inps.read(b,0,c);
			fos.write(b, 0, len);			
		}
		fos.close();		
		inps.close();
	}
	/*
	private IFile saveToResourceFileAndClose(InputStream inps,String fileName) throws CoreException, IOException, InterruptedException{
		IFile outf = HascLoggerServer.getOutputBaseFolder().getFile(fileName);
		ProgressMonitorForBlocking blk = new ProgressMonitorForBlocking();
		if (outf.exists()) {
			outf.setContents(inps, true, false, blk);
		}else{
			outf.create(inps, true, blk);
		}
		blk.awaitDone();
		inps.close();
		LOG.debug("wrote file:"+fileName);
		return outf;
	}
*/
}
